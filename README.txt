
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * USe

INTRODUCTION
------------

Current Maintainer: Bastlynn http://drupal.org/user/275249

jQuery++ is a set of plugins for the down and dirty features jQuery doesn't
already cover. This module provides access to those plugins for your own work. It
doesn't do anything on its own.

Much credit for the basic structure of this module can be given to jquery_ui.

REQUIREMENTS
------------

* jQuery Update set to jQuery 1.7.

* The jQuery++ library.

INSTALLATION
------------

1. Unzip and copy this directory to your sites/all/modules directory. 

2. Download jQuery++ from https://github.com/downloads/jupiterjs/jquerypp/jquerypp-1.0b.zip.

3. Unzip the contents into /sites/all/modules/jquery_pp/jquerypp/ .
  So that all of jQuery++ js files are located in /sites/all/modules/jquery_pp/jquerypp/*.js 

4. Enable the module.

USE
---

In order to use the available plugins in your own modules or themes.

1) Make sure jquery_pp is listed as a dependancy in your .info file.

2) To add a jQuery++ plugin to your file, call jquerypp_add($files).

  Ex:
    jquery_pp_add('animate');
    jquery_pp_add(array('animate', 'event.default', 'event.pause'));

  Be sure to look for the dependancies listed for each plugin and include those as well.